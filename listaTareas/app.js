"use strict";
class TodoItem {
    constructor(task, isCompleted) {
        this.task = task;
        this.isCompleted = isCompleted;
    }
}
class TaskManager {
    constructor() {
        this.tasks = [];
    }
    addTask(task) {
        const newItem = new TodoItem(task, false);
        this.tasks.push(newItem);
    }
}
class HTMLHelper {
    static createTaskItem(task) {
        const listItem = document.createElement('li');
        const checkbox = document.createElement('input');
        checkbox.addEventListener('change', () => {
            if (checkbox.checked) {
                task.isCompleted = true;
                displayTasks();
            }
        });
        const label = document.createElement('label');
        checkbox.type = 'checkbox';
        label.innerHTML = task.task;
        listItem.appendChild(checkbox);
        listItem.appendChild(label);
        return listItem;
    }
}
const taskInput = document.getElementById('new-task');
const addButton = document.getElementById('add-task');
const incompletedTasksHolder = document.getElementById('incompleted-tasks');
const completedTasksHolder = document.getElementById('completed-tasks');
const taskManager = new TaskManager();
addButton.addEventListener("click", () => {
    taskManager.addTask(taskInput.value);
    displayTasks();
    clear();
});
function displayTasks() {
    completedTasksHolder.innerHTML = '';
    incompletedTasksHolder.innerHTML = '';
    taskManager.tasks.forEach(element => {
        var listItem = HTMLHelper.createTaskItem(element);
        if (element.isCompleted) {
            completedTasksHolder.appendChild(listItem);
        }
        else {
            incompletedTasksHolder.appendChild(listItem);
        }
    });
}
function clear() {
    taskInput.value = '';
}
